use std::{time::{
    Duration,
    SystemTime
}, f64};

use self::{common::*, constants::{MONTHS_OF_THE_COMMON_YEAR}};

pub mod constants;
pub mod common;


/// Struct to parse Epoch time
pub struct
Date {
    utc    : Duration,
    year   : u32,
    month  : u32,
    weekday: u32,
    day    : u32,
    hour   : u32,
    minutes: u32,
    seconds: u32,
    millisecond: u32,
}

impl Date {
    pub fn new(now: Duration) -> Date {
        Date {
            utc    : now,
            year   : 0,
            month  : 0,
            weekday: 0,
            day    : 0,
            hour   : 0,
            minutes: 0,
            seconds: 0,
            millisecond: 0
        }
    }
    /// Parse Unix time second to Year/Months/Days/Hours/Minute/Seconds
    pub fn parse(&mut self) {
        let total_years          : f64 = epoch_to_years(self.utc);
        let truncated_total_years: f64 = total_years.trunc();
        let total_leap           : f64 = curr_leap_counter(total_years);
        
        let year_days : f64 = year_days_perc(total_years);
        let year_days : f64 = year_days * (365.0 - total_leap) as f64;

        self.set_curr_year(truncated_total_years);
        self.set_curr_mouth_and_day(&year_days);
        self.set_curr_time(&year_days);
        self.set_weekday();
    }

    fn set_curr_year(&mut self, total_years: f64) {
        self.year = 1970 + total_years as u32;
    }

    fn set_curr_mouth_and_day(&mut self, year_days: &f64) {
        let year_days = *year_days as u32;
        (self.month, self.day) = fetch_month_and_day_of_the_year(
            self.is_leap(),
            &year_days
        );
    }

    fn set_weekday(&mut self) {
        self.weekday = fetch_weekday(&self.year, &self.month, &self.day);
    }

    fn set_curr_time(&mut self, &year_days: &f64) {
        let date_time_in_decimal = year_days  - year_days.trunc();
        let time = match fetch_time(&date_time_in_decimal) {
            Ok(time) => time,
            Err(err) => panic!("{}", err)
        };

        self.hour        = time.0;
        self.minutes     = time.1;
        self.seconds     = time.2;
        self.millisecond = time.3;
    }

    fn is_leap(&self) -> bool {
        (self.year % 2) == 0
    }


}
