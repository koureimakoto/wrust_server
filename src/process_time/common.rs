use std::{time::Duration, fmt::Error};
use super::constants::{
    COMMON_MIDYEAR,
    YEAR_SECONDS,
    LEAP_INTERVAL, MAX_MONTHS_SIZE, MONTHS_OF_THE_COMMON_YEAR, MAX_HOUR, MAX_MIN, MAX_MILLISEC
};

#[deprecated = "Inefficient! You should use fetch_month_and_day_of_the_year() instead"]
pub fn
fetch_month_of_year(leap: bool, year_days: &u32, months: &[[u32; 5]; 2]) -> u8 {
    let leap: u32 = match leap {
        true => 1,
        _    => 0
    };
    let shim: u8 = 5;

    if *year_days < (COMMON_MIDYEAR + leap) {
        months_counter(&months[0], &year_days)
    } else {
        shim + months_counter(&months[1], &year_days)
    }
}

#[deprecated = "Inefficient! You should use fetch_month_and_day_of_the_year() instead"]
pub fn
months_counter(months: &[u32; 5], year_days: &u32) -> u8 {  
    if year_days > &(months[4] + 31) {
        panic!("Limit of year days exceeded!")
    }
    
    if *year_days <= 31 {
        return 1
    } 
    
    let mut month_count: u8 = 2;
    for days in months.iter() {
        if  year_days >= days {
            month_count += 1;
        }
    }
    month_count
}

pub fn
fetch_month_and_day_of_the_year(leap: bool, year_days: &u32) -> (u32, u32) {
    if year_days > &MONTHS_OF_THE_COMMON_YEAR[MAX_MONTHS_SIZE] {
        panic!("Max Limit of year days exceeded!")
    } else
    if *year_days < 1 {
        panic!("Min value of year days is 01!")
    }

    let leap: u32 = match leap {
        true => 1,
        _    => 0
    };

    let mut month: usize = 0;

    'found: for (index, days) in MONTHS_OF_THE_COMMON_YEAR.iter().enumerate() {
        if year_days < &(days + leap){
            month = index;
            break 'found
        } 
    }

    match month {
        0 => ((month + 1) as u32, *year_days as u32),
        1 => ((month + 1) as u32, (year_days - &MONTHS_OF_THE_COMMON_YEAR[ month - 1 ]) as u32),
        _ => ((month + 1) as u32, (year_days - &MONTHS_OF_THE_COMMON_YEAR[ month - 1 ] - leap)  as u32 )
    }
}



type Time = (u32, u32, u32, u32);

pub fn
fetch_time(date_time_in_decimal: &f64) -> Result<Time, &str> {
    if *date_time_in_decimal >= 1.0f64 || *date_time_in_decimal <= 0.0f64 {
        Err("This number is greater than 1 or less than zero")
    }
    else {
        let hour     : f64 = *date_time_in_decimal * MAX_HOUR;
        let min      : f64 = (hour - hour.trunc()) * MAX_MIN;
        let sec      : f64 = (min  - min.trunc() ) * MAX_MIN;
        let milli_sec: f64 = (sec  - sec.trunc() ) * MAX_MILLISEC;
        
        Ok((
            hour      as u32,
            min       as u32,
            sec       as u32,
            milli_sec as u32
        ))
    }
}

// based on https://wiki.c2.com/?PerpetualCalendarAlgorithm 
pub fn
fetch_weekday(year: &u32, month: &u32, day: &u32) -> u32 {
    let mut day   = *day;
    let mut month = *month;
    let mut year  = *year;

    day += if month < 3 {
        year -= 1;
        year + 1
    } else {
        year - 2 
    } + 4;

    month = (23 * month)/ 9; 
    year  = year/4 - year/100 + year/400;

    (day + month + year) % 7
}

pub fn
convert_numerical_weekday_to_text(numerical_weekday: &u32) -> &str {
    match numerical_weekday {
        0 => "Sun",
        1 => "Mon",
        2 => "Tue",
        3 => "Wed",
        4 => "Thu",
        5 => "Fri",
        6 => "Sat",
        _ => panic!("Invalidade numerical day of week")
    }
}

pub fn
epoch_to_years(epoch: Duration) -> f64 {
    epoch.as_secs() as f64 / YEAR_SECONDS as f64
}

pub fn
curr_leap_counter(total_years: f64) -> f64 {
    total_years / LEAP_INTERVAL
}

pub fn
year_days_perc(total_years: f64) -> f64 {
    let year = total_years - total_years.trunc();

    if year >= 0.0 {
        year
    } else {
        panic!("Year Percentage was out of range.")
    }
}

#[cfg(test)]
#[path ="common_test.rs"]
pub mod common_test;